<?php
/**
	Plugin Name: Post To Widget
	Plugin URI:  https://bitbucket.org/bicarbona/post-to-widget
	Description: Post To Widget
	Version:     0.1.3
	Author:      bicarbona
	Author URI:  
	License:     GPLv2+
	Text Domain: post_to_widget
	Domain Path: /languages
 	Bitbucket Plugin URI: https://bitbucket.org/bicarbona/post-to-widget
 	Bitbucket Branch: master
*/

// Launch the plugin.
add_action( 'plugins_loaded', 'custom_post_widget_plugin_init' );

// Load the required files needed for the plugin to run in the proper order and add needed functions to the required hooks.
function custom_post_widget_plugin_init() {
	// Load the translation of the plugin.
	load_plugin_textdomain( 'custom-post-widget', false, 'custom-post-widget/languages' );
	add_action( 'widgets_init', 'custom_post_widget_load_widgets' );
}

// Loads the widgets packaged with the plugin.
function custom_post_widget_load_widgets() {
	require_once( 'post-widget.php' );
	register_widget( 'custom_post_widget' );
}

// require_once( 'meta-box.php' );
// require_once( 'popup.php' );